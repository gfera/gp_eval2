<?php

include __DIR__."/../models/base/iForma.php";
include __DIR__."/../models/Cuadrado.php";
include __DIR__."/../models/Triangulo.php";
include __DIR__."/../models/Circulo.php";

class ShapeFactory
{
    public static function build($type, $args)
    {

        switch ($type) {
            case 'cuadrado':
            	return new Cuadrado(...$args);
			case 'triangulo':
				return new Triangulo(...$args);
			case 'circulo':
				return new Circulo(...$args);
        }
    }
}
