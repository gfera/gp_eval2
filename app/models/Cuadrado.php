<?php

class Cuadrado implements iForma {
	private $lado;
	private $superficie;
	/*-----------------------------------------*/
	function __construct(float $lado){
		$this->lado = $lado;
		$this->superfice = $lado*$lado;
	}
	/*-----------------------------------------*/
	public function getTipo(){
		return 'cuadrado';
	}
	public function getSuperficie(){
		return $this->superfice;
	}
	public function getBase(){
		return $this->lado;
	}
	public function getAltura(){
		return $this->lado;
	}
	public function getDiametro(){
		throw new Exception("Medida no aplicable para un cuadrado", 1);

	}

}
