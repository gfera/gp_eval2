<?php

class Circulo implements iForma {
	private $superficie;
	private $diametro;
	/*-----------------------------------------*/
	function __construct(float $diametro){
		$this->diametro = $diametro;
		$this->superfice = pow(pi() * ($this->diametro/2),2);
	}
	/*-----------------------------------------*/
	public function getTipo(){
		return 'circulo';
	}
	public function getSuperficie(){
		return $this->superfice;
	}
	public function getBase(){
		throw new Exception("Medida no aplicable para un círculo", 1);
	}
	public function getAltura(){
		throw new Exception("Medida no aplicable para un círculo", 1);
	}
	public function getDiametro(){
		return $this->diametro;

	}
}
