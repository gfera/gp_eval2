<?php
interface iForma {
	public function getTipo();
	public function getSuperficie();
	public function getBase();
	public function getAltura();
	public function getDiametro();
}
