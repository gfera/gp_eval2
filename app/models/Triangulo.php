<?php

class Triangulo implements iForma {
	private $base;
	private $altura;
	private $lado;
	private $superficie;
	/*-----------------------------------------*/
	function __construct(float $lado){
		$this->lado = $lado;
		$this->base = $lado;
		$this->altura = sqrt(pow($lado,2) - pow($lado/2,2));
		$this->superfice = $this->altura * ($lado/2);
	}
	/*-----------------------------------------*/
	public function getTipo(){
		return 'triangulo';
	}
	public function getSuperficie(){
		return $this->superfice;
	}
	public function getBase(){
		return $this->base;
	}
	public function getAltura(){
		return $this->altura;
	}
	public function getDiametro(){
		throw new Exception("Medida no aplicable para un triángulo", 1);

	}
}
