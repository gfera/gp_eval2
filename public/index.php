<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require '../app/helpers/factory.php';

$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];

$app = new \Slim\App($configuration);
$app->get('/forma/{name}/{size}', function (Request $request, Response $response) {
    $name = $request->getAttribute('name');
    $size = $request->getAttribute('size');

    $o = ShapeFactory::build($name, array($size));
    echo 'Tipo:'.$o->getTipo();
    echo '<br>Superficie:'.$o->getSuperficie();
	echo '<br>Base:';
    try {
        echo $o->getBase();
    } catch (Exception $e) {
		echo $e->getMessage();
    }

	echo '<br>Altura:';
    try {
        echo $o->getAltura();
    } catch (Exception $e) {
		echo $e->getMessage();
    }
	echo '<br>Diametro:';
    try {
        echo $o->getDiametro();
    } catch (Exception $e) {
		echo $e->getMessage();
    }

    //$response->getBody()->write(json_encode($o));
    return $response;
});

$app->get('/', function (Request $request, Response $response) {
    $body = 'Rutas para ejecutar pruebas:<br>';
    $body .= "<a href='/forma/cuadrado/10'>Cuadrado</a></br>";
    $body .= "<a href='/forma/triangulo/10'>Triangulo</a></br>";
    $body .= "<a href='/forma/circulo/10'>Circulo</a></br>";
    $response->getBody()->write($body);

    return $response;
});

$app->run();
